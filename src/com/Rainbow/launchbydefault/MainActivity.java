package com.Rainbow.launchbydefault;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.Tracker;


import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
 
public class MainActivity extends Activity {
    private ListView mListAppInfo;
    private Context mContext = null;
    private Tracker tracker  = null; // use for track event, exception..etc.
    private int iDispatchTime  = 3; // determine how long send the information to Google
    private double dSampleRate  = 100;  // determine how much information to be send
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set layout for the main screen
        setContentView(R.layout.activity_main);
 
        // load list application
        mListAppInfo = (ListView)findViewById(R.id.lvApps);
        // create new adapter
        AppInfoAdapter adapter = new AppInfoAdapter(this, Utilities.getInstalledApplication(this), getPackageManager());
        // set adapter to list view
        mContext = this;
        mListAppInfo.setAdapter(adapter);
        analytics();
        admob();
        // implement event when an item on list view is selected
        mListAppInfo.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {
                // get the list adapter
                AppInfoAdapter appInfoAdapter = (AppInfoAdapter)parent.getAdapter();
                // get selected item on the list
                ApplicationInfo appInfo = (ApplicationInfo)appInfoAdapter.getItem(pos);
                // launch the selected application
                Utilities.launchApp(parent.getContext(), getPackageManager(), appInfo.packageName);
            }
        });
    }

	private void analytics() {
	      EasyTracker.getInstance().setContext(mContext); // initial
	      
	      // start a Tracker, you must to do this thing otherwise you can't send any tracker to Google.
	      EasyTracker.getInstance().activityStart((Activity)mContext); // Add this method.
	      
	      // �����������tracker
	      tracker = EasyTracker.getTracker();
	      
	      // 設���Sample Rate, sample rate����������������������app�����人使������
	      // �����������以���己設��������������������evice�����������������Google
	      // ������設���100%�����就���說�������evice�����������������
	      tracker.setSampleRate(dSampleRate);
	      
	      // 設���dispatch time
	      // ���������������������������說�����trackEvent, trackException�����
	      // �������������������������������������以������������ipatch)
	      GAServiceManager.getInstance().setDispatchPeriod(iDispatchTime);
	      
	      
	      // track event
	      tracker.trackEvent("UI_ACTION", "buttonClick", "labelCreatedByKen", 0l);
	      
	      // track view, 追蹤������activity使�����������������������������	      tracker.trackView("Hidden Cameras");
	      
	      // track exception, ������������������track exception
	      // �����以���trackException������try{}cathc(){};�����
	      tracker.trackException("ExceptionCreatedByKen", false);
		
	}

	private void admob() {
		Display d = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		AdView adView = new AdView(this, AdSize.BANNER, "ca-app-pub-4668589427063896/9831941164");
	    // �d�� LinearLayout (���]�z�w�g����)
	    // �ݩʬO android:id="@+id/mainLayout"
	    LinearLayout layout = (LinearLayout)findViewById(R.id.mainLayout);
	    layout.getLayoutParams().width = d.getWidth();
	    // �b�䤤�[�J adView
	    layout.addView(adView);

	    // �ҥΪx�νШD�A���H�s�i�@�_���J
	    adView.loadAd(new AdRequest());
		
	}
}