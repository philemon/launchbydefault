package com.Rainbow.launchbydefault;

import java.util.ArrayList;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;
 
public class Utilities {
 
    /*
     * Get all installed application on mobile and return a list
     * @param   c   Context of application
     * @return  list of installed applications
     */
    public static List getInstalledApplication(Context c) {
    	List<ApplicationInfo> defaultpackages = new ArrayList<ApplicationInfo>();
    	List<PackageInfo> packages = c.getPackageManager()
                .getInstalledPackages(0);
    	List<IntentFilter> filters = new ArrayList<IntentFilter>();
        List<ComponentName> activities = new ArrayList<ComponentName>();
        String info = "";
        int nPref = 0, nFilters = 0, nActivities = 0;
        PackageInfo pkg = null;
        for (int i = 0; i < packages.size(); i++) {
            pkg = packages.get(i);
            nPref = c.getPackageManager().getPreferredActivities(filters,
                    activities, pkg.packageName);
            nFilters = filters.size();
            nActivities = activities.size();
            if (nPref > 0 || nFilters > 0 || nActivities > 0) {
            	try {
					defaultpackages.add(c.getPackageManager().getApplicationInfo(pkg.packageName, nPref));
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        return defaultpackages;
    }
 
    /*
     * Launch an application
     * @param   c   Context of application
     * @param   pm  the related package manager of the context
     * @param   pkgName Name of the package to run
     */
    public static boolean launchApp(Context c, PackageManager pm, String pkgName) {
        // query the intent for lauching
        Intent intent = pm.getLaunchIntentForPackage(pkgName);
        // if intent is available
        if(intent != null) {
            try {
                // launch application
                c.startActivity(intent);
                // if succeed
                return true;
 
            // if fail
            } catch(ActivityNotFoundException ex) {
                // quick message notification
                Toast toast = Toast.makeText(c, "Application Not Found", Toast.LENGTH_LONG);
                // display message
                toast.show();
            }
        }
        // by default, fail to launch
        return false;
    }
}
